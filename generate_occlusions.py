from cv2 import *
import numpy as np


def place_occ(img, landmarksList, i):
    return {"eyes1": [landmarksList[0][18],
                      landmarksList[0][25],
                      (landmarksList[0][25][0], landmarksList[0][28][1]),
                      (landmarksList[0][18][0], landmarksList[0][28][1])],
            "eyes2": [(landmarksList[0][0][0], landmarksList[0][19][1]),
                      (landmarksList[0][16][0], landmarksList[0][24][1]),
                      (landmarksList[0][16][0], landmarksList[0][29][1]),
                      (landmarksList[0][0][0], landmarksList[0][29][1])],
            "eyes3": [(0, 0),
                      (img.shape[1], 0),
                      (img.shape[1], landmarksList[0][29][1]),
                      (0, landmarksList[0][29][1])],
            "mouth1": [(landmarksList[0][4][0], landmarksList[0][3][1]),
                       (landmarksList[0][12][0], landmarksList[0][13][1]),
                       (landmarksList[0][12][0], landmarksList[0][11][1]),
                       (landmarksList[0][4][0], landmarksList[0][5][1])],
            "mouth2": [(landmarksList[0][0][0], landmarksList[0][8][1]),
                       (landmarksList[0][16][0], landmarksList[0][8][1]),
                       landmarksList[0][13],
                       landmarksList[0][29],
                       landmarksList[0][3]],
            "mouth3": [(0, landmarksList[0][1][1]),
                       (img.shape[1], landmarksList[0][15][1]),
                       (img.shape[1], img.shape[0]),
                       (0, img.shape[0])],
            "left": [(0, 0),
                     (landmarksList[0][29][0], 0),
                     (landmarksList[0][8][0], img.shape[0]),
                     (0, img.shape[0])],
            "right": [(landmarksList[0][29][0], 0),
                      (img.shape[1],0),
                      (img.shape[1], img.shape[0]),
                      (landmarksList[0][8][0], img.shape[0])]
            }


def texture_occ(img, pts, occ):
    imgOcc=img.copy()
    if occ=="black":
        fillConvexPoly(imgOcc, pts, [0, 0, 0])
        return imgOcc
    elif occ=="white":
        fillConvexPoly(imgOcc, pts, [255, 255, 255])
        return imgOcc
    elif occ=="blur":
        blurRegion = imgOcc.copy()
        height, width = imgOcc.shape
        mask = np.zeros((height, width), np.uint8)
        fillConvexPoly(mask, pts, [255, 255, 255])
        blurRegion = GaussianBlur(blurRegion, (0, 0), 10)
        fg = bitwise_or(blurRegion, blurRegion, mask=mask)
        mask = bitwise_not(mask)
        bk = bitwise_or(imgOcc, imgOcc, mask=mask)
        imgOcc = bitwise_or(fg, bk)
        return imgOcc
    elif occ=="saltAndPepper":
        mask = np.zeros(img.shape, np.uint8)
        saltAndPepper = np.zeros(img.shape, np.uint8)
        fillConvexPoly(mask,pts,[1,1,1])
        randn(saltAndPepper, 128, 30)
        newMask = saltAndPepper * (mask.astype(saltAndPepper.dtype))
        fillConvexPoly(imgOcc, pts, [0, 0, 0])
        imgOcc = imgOcc + (newMask.astype(img.dtype))
        return imgOcc